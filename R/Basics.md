# Data Structure
## Data Type

- numeric
	- `est <- 1.2`
- character
	- `name <- "John"`
	- `as.character(est)`
- logical
## Vector

- Concatenation `c(1,2,3,4)`
- Parameters
	- `length()`
	- `class()`
	- `mode()`
- Vectors of Systematic numbers
	- `x <- 1:10`
	- sequence
		- `seq(from=0,to=20,by=5)`
		- `seq(from=0,to=20,length=5)`
	- repeat
		- `rep(12:14,times=2)`
		- `rep(12:14,each=2)`
- Non-numeric vectors
	- `PERSON <- c("A","B","C")`
	- `person <- letters[1:11]` 大写字母用 `LETTERS`
	- `class(person)`
	- `paste("My", "Job")` 默认组内用空格分断
	- `paste("x",1:5,sep="_",collapse=";")`
	- `paste("Today is",date())`
	- `cat("Today is",date())`
- Operations
	- 按元素 + - * / \%\%(模) %/%(整除)
	- recycle（循环补齐）
		- `c(1,2,4)+c(6,0,9,20,22)` = `7,2,13,21,24`
- Subsetting
	- `r <- seq(3,100,7)`
	- `r[4]`
	- `r[c(2,4,4)]`
	- `r[-4]`
	- `r[-c(2,4,6)]`
	- `r[-length(r)]`
	- `r[r/2==trunc(r/2)]`
- Filtering
	- `sub <- c(6,2:3,NA,14)`
	- `subset(sub,sub>5)`
- Vectorizing
	- `x <- 1:10`
	- `y <- ifelse(x%%2==0,1,0)`
	- `[1] 0,1,0,1,0,1,0,1,0,1`
- Functions
	- `any(r>50)`
	- `all(r<75)`
	- `identical(r,c(49,81))`
	- `r==c(49,81)`
	- `which(r^2<200)`
	- `which.min(x);which.max(x)`
	- `min(x);max(x);range(x)`
	- `median(x);quantile(r);quantile(r,0.2)`
	- `sum(r);prod(x)`
	- `summary(r);mean(r);sd(r);var(r)`
	- `sort(r);order(x)`

## Matrix

- Creation
	- `a <- matrix(1:9, nrow=3, byrow=T)`
- Parameters
	- `length(a)`
	- `class(a)`
	- `dim(a)`
	- `nrow(a)`
	- `ncol(a)`
	- `mode(a)`
	- `attributes(a)`
- Assigning
	- `a[2,2]=0`
- Indexing
	- `a[2,2]`
	- `a[2,];a[,2]`
	- `a[-2,];a[,-2]`
	- `a[2:3,];a[,2:3]`
- Filtering
	- `a[a[,2]>5,]`
- Concatenating
	- `P=matrix(1:4,nrow=2)`
	- `Q=matrix(6:9,nrow=2)`
	- `rbind(P,Q)`
	- `cbind(P,Q)`
	- `rbind(P,99:100)`
- Naming
	- `x=cbind(c(0,1,1,0),c(78,88,94,64))`
	- `colnames(x)=c("is_female","score")`
	- `rownames(x)=c("Aaron","Betty","Cathy","David")`
	- `x[,"score"];x["Cathy",];x["David","score"]`
- Operations
	- + - * /
	- `t(r);det(r)`
	- `%*% %o% diag()` 内积、外积、对角阵
	- `solve()` 解线性方程
	- `eigen()`
	- `svd()`
	- `lsfit.sol()` 最小二乘拟合

## Array

- Creation
	- `first=cbind(c(86,91,80),c(30,85,50))`
	- `second=cbind((76,41,70),c(59,78,66))`
	- `test=array(data=c(first,second),dim=c(3,2,2))`
- Parameters
	- `length();class();attributes()`
- Folding a vector into an array
	- `arr=1:8;dim(arr)=c(2,2,2)`
- Indexing
- Filtering
- Concatenating

## Factor

- Creating
	- `sex=c("M","F","M","M","F")`
	- `sexf=factor(sex)`
	- `glf=gl(3,1,15)` gl 函数，从1到3每循环出现一次一直到15个数
- Parameters
	- `levels(sexf)` 因子水平，就是类别
	- `length(glf)`
	- `mode(glf)`
	- `class(glf)`
	- `is.factor(glf)`
	- `attr(glf, "levels")`
- tapply 应用一个聚合计算函数
	- `score=c(174,165,180,171,160)`
	- `tapply(score,sexf,FUN=mean)`
	- `tapply(score,sexf,FUN=range)`
- `split(score,sexf)`
- `by(score,sexf,FUN=range)`
## List

- Creating
	- `j=list(name="Joe",salary=55000,party=T)`
- Parameters
	- `length(j)`
	- `names(j)`
	- `str(j)`
- Indexing
	- `j$salary`
	- `j[["salary"]]`
	- `j[[2]]`
- Inserting, Updating and Deleting
	- `j$sex="Male"`
	- `j$party=NULL`
	- `j$salary=100000`
- apply
	- `lapply(list(1:10,40:90),median)` 返回一个列表
	- `sapply(list(1:10,40:90),median)` 返回一个数组

## Data Frame

- Creating
	- `kids=c("Jack","Jill")`
	- `ages=c(12,10)`
	- `d=data.frame(kids,ages,stringsAsFactors=F)`
- Indexing
	- `d[,1];d[2,];d[,"kids"];d[,"ages"];d[[1]];d$kids`
- Concatenating
	- `rbind(d,list("Laura",19))`
	- `cbind(d,quiz=c(4.5,5))`
- apply
	- `lapply(d,sort)`
	- `sapply(d,sort)`
- function
	- `merge()`


# Pipeline operators

`%>%` 向右操作符 `Ctrl+Shift+M`

传递表达式或数据。

```R
set.seed(123)
n1<-rnorm(10000)        
n2<-abs(n1)*50        
n3<-matrix(n2,ncol = 100) 
n4<-round(rowMeans(n3))
hist(n4%%7)

set.seed(123)hist(round(rowMeans(matrix(abs(rnorm(10000))*50,ncol=100)))%%7)
```

```R
library(magrittr)
set.seed(123)
rnorm(10000) %>%
  abs %>% `*` (50)  %>%
  matrix(ncol=100)  %>%
  rowMeans %>% round %>% 
  `%%`(7) %>% hist
```

`%T>%` 向左操作符

将数据同时传给下一个和下下个，用于中间打印图像等过程。

```R
rnorm(10000) %>%
  abs %>% `*` (50)  %>%
  matrix(ncol=100)  %>%
  rowMeans %>% round %>% 
  `%%`(7) %T>% hist %>% sum
```

`%$%` 解释操作符

传递属性名。

```R
# . 表示完整的数据对象
data.frame(x=1:10,y=rnorm(10),z=letters[1:10]) %$% .[x>5,]
```

`%<>%` 符合赋值操作符

将结果写回最左侧的对象（覆盖原来的值）。

```R
x<-rnorm(10)
x %<>% abs %>% sort
```