# Basics

`ls()` see which object names that have already used

`element-wise operations` will repeat a short vector to do with the two of uneven lengths

`%*%` inner multiplication, `%o%` outer multiplication

`t` transpose, `det` get determinant

`round` , `factorial` , `mean`, `sum`  

`sample(x=vec, size=int, replace=bool, prob=vec)` 

`args` , `?` , `??` , 

`replicate(times=int,FUN=func)`

# Objects

![data-type|400](https://rstudio-education.github.io/hopr/images/hopr_0306.png)

`c()` create a automic vector, also a single value is an automic vector

`length` , `L` means integer, any number means double (or numeric), `i` means an imaginary term

`attributes`, `names`, `class` , `unclass` 

`dim` tansform an automic vector into an n-dimensional array

`unique` return every unique term in a vector

`str(df)` , `data.frame(...,stringsAsFactors=F)` , `head`

`write.csv(...,row.names=F)` 

`getwd` , `setwd` to get/set working directory

# Notation

index:
- positive int
- negative int
- zero
- blank space
- logical value
- name

select `[i,j]` , use `[i,j,drop=F]` to prevent single col/row be a vector

# Environments

`parenvs(all=T)`

# Programs

`unname` remove names attribute and return a copy of the object
# S3

`attributes`

`attr(obj, "attr")=c()`

`structure(obj, class="", attr=c(), ...)`

`methods`

`print.obj_class = FUN`
# Loops

`expand.grid(vec1, vec2, ..., vecn, stringAsFactors=F)` every combination of  n vectors' elements

`for(x in vec)` , `while(cond)` , `repeat`
# Speed

- vectorized code
- `winnings = vector(length = 1000000)`

