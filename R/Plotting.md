# Principles

1. Show comparisons
2. Show causality, mechanism, and explanation
3. Show multivariate data
4. Integrate multiple modes of evidence
5. Describe and document the evidence
6. Content matters the most

# R graphics system

- Basic graphics
- Lattice(trellis) graphics
- ggplot2

# ggplot2

`ggplot(Data, aes(x=A,y=B)) + geom_point()`

## Data

- usually using variables in a data frame
- vectors: continuous or discrete
	- one vector
	- two vectors with the same length
- context

`ggplot(data) + geom_point(aes(x=x,y=y))`
`ggplot() + geom_point(data=data,aes(x=x,y=y))`

## Aesthetic

- x and y coordinates
- colour, size, shape, and transparency

`ggplot(data,aes(x=x,y=y)) + geom_point()`

## Geometry

|      Name      |               Description                |
|:--------------:|:----------------------------------------:|
|   geom_point   |               Scatterplot                |
|    geom_bar    |                 Bar plot                 |
| geom_histogram |                Histogram                 |
|  geom_density  |      Probability distribution plot       |
|  geom_boxplot  |          Box-and-whiskers plot           |
|   geom_text    |      Textual annotations in a plot       |
| geom_errorbar  |                Error bars                |
|   geom_line    | Connect observations, in order by x value |
|  geom_smooth   |      Add a smoothed condition mean       |
|                |                                          |

### How to choose a geometry

|    What do you want to present    |                                        What's your choice                                         |          Statistics           |
|:---------------------------------:|:-------------------------------------------------------------------------------------------------:|:-----------------------------:|
|       1 sample, 1 variable        |                        histogram, density curve, cumulative density curve                         |                               |
|       2 samples, 1 variable       | scatter plot + error bar, bar plot + error bar, box plot, density curve, cumulative density curve | t.test(), wilcox.test(), lm() |
| 2 samples, 1 variable, proportion |                                        bar plot, pie plot                                         |  fisher.test(), binom.test()  |
|       1 sample, 2 variables       |                                          scatter + line                                           |       lm(), cor.test()        |
|       1sample, >2 variables       |                                          scatter + line                                           |             lm()              | 


# Practical

```R
library(ggplot2)

# sample data
setwd()
data=read.csv("xxx.csv")
dim(data)
head(data)

# histogram
p=ggplot(data, aes(x=bmi)) + geom_hitogram()

# boxplot
p=ggplot(data, aes(gender,bmi,fill=gender)) + geom_boxplot()

# multi-layers
g=ggplot(data,aes(bmi,sbp))
g+geom_point()
g+geom_point()+geom_smooth()
g+geom_point()+geom_smooth(method="lm")
## modify aes
g+geom_point(color="steelblue",size=4,alpha=1/2)
g+geom_point(aes(color=gender),size=4,alpha=1/2)
## modify labels
g+geom_point(aes(color=gender))+labs(title="NHANES")+labs(x=expression("BMI (kg/"*m^2*")"), y="SBP (mmHg)")
## change the theme
g+geom_point(aes(color=gender))+labs(title="NHANES")+labs(x=expression("BMI (kg/"*m^2*")"), y="SBP (mmHg)")+theme_bw()
## facets
g+geom_point()+facet_grid(. ~ gender)+xlim(18,40)+ylim(80,200)+geom_smooth(method="lm")

# save a plot
ggsave(nhanes, file="bmi_sbp.pdf/png")
```

![](./attachments/Pasted%20image%2020240121170340.png)

