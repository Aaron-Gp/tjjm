---
author: Aaron Gao
title: R Graphics
time: 2024-03-19 Tue
tags:
  - template
obsidianUIMode: source
obsidianEdtingMode: preview
description: details
---

# R Basics

```R
# install packages
install.packages(c("ggplot2", "tidyverse"))
# load packages
library(ggplot2)
# upgrade packages
update.packages(ask=F)
# load a delimited text data file
data = readr::read_csv("datafile.csv", header=F, stringAsFactor=F)
names(data)=c("C1","C2","C3")
data$sex = factor(data$sex)
str(data)
# load an excel file
data=readxl::read_excel("datafile.xlsx", sheet=1, col_names=F)
# load a SPSS/SAS/Stata file
install.packages(c("foreign","haven","readstata13"))
# chain functions together
install.packages(c("magrittr")) # also included in tidyverse

```

# Quickly Exploring Data

```R

```