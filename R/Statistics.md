# Sampling

- with numbers
	- `sample(1:10,7,replace=F)`
- with characters
	- `coin_toss=sample(c("F","B"),10,replace=T)`
- with certain probability
	- `weekdays_52=sample(c("Mon","Tue","Wed"."Thu","Fri"),52,replace=T,prob=c(0.45,0.15,0.05,0.10,0.25))

# Test

- 二分变量
	- 完全随机设计
		- 卡方检验
	- 配对设计
		- McNemar检验
- 有序变量：（秩和检验）
	- 完全随机设计
		- 两组——Wilcoxon秩和检验
		- 多组——Kruskal-Wallis秩和检验
	- 配对设计
		- 符号秩和检验
- 多分类变量
	- 完全随机设计
		- 卡方检验

# Correlation

`cor.test(vec_x,vec_y,method="pearson/spearman/kendall")`

# Linear Regression

`lm(x ~ y, data=df)`

```R
# Q-Q Plot
qqnorm(resid(lm_BMI_SBP),pch=16,col="red")
qqline(resid(lm_BMI_SBP),col="purple")
```