`https://tidyverse.tidyverse.org/`

![data-science|400](https://r4ds.hadley.nz/diagrams/data-science/base.png)
# readr




# \<tidy-select\>

- `var1:var10`: variables lying between `var1` on the left and `var10` on the right.
- `starts_with("a")`: names that start with `"a"`.
- `ends_with("z")`: names that end with `"z"`.
- `contains("b")`: names that contain `"b"`.
- `matches("x.y")`: names that match regular expression `x.y`.
- `num_range(x, 1:4)`: names following the pattern, `x1`, `x2`, ..., `x4`.
- `all_of(vars)`/`any_of(vars)`: matches names stored in the character vector `vars`. `all_of(vars)` will error if the variables aren't present; `any_of(var)` will match just the variables that exist.
- `everything()`: all variables.
- `last_col()`: furthest column on the right.
- `where(is.numeric)`: all variables where `is.numeric()` returns `TRUE`.

As well as operators for combining those selections:

- `!selection`: only variables that don't match `selection`.
- `selection1 & selection2`: only variables included in both `selection1` and `selection2`.
- `selection1 | selection2`: all variables that match either `selection1` or `selection2`.
