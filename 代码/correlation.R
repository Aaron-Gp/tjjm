library(corrplot)
library(palmerpenguins)

df = penguins %>% 
  na.omit()
df$species=as.numeric(df$species)
df$sex=as.numeric(df$sex)
df$island=as.numeric(df$island)
m = cor(x=df)
corrplot.mixed(m,
               lower = 'shade',
               upper = 'pie',
               order = 'hclust')